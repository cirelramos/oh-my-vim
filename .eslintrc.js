module.exports = {
  globals: {
    window: true,
    document: true
  },
  env: {
    es6: true,
    node: true,
    mocha: true,
    jquery: true
  },
  parser: 'babel-eslint',
  parserOptions: {
    allowImportExportEverywhere: true,
    ecmaVersion: 6,
    sourceType: 'module'
  },
  plugins: [
    'babel',
    'react',
    'jsx-a11y'
  ],
  extends: [
    'eslint:recommended',
    'airbnb'
  ],
  rules: {
    indent: ['error', 2],
    'no-tabs': 2,
    'indent-size': [true, 4],
    'react/jsx-indent-props': 'warn',
    'react/prefer-stateless-function': 'off',
    'react/jsx-closing-bracket-location': 'warn',
    'object-curly-spacing': 'warn',
    'max-len': [1, 200, 1], // specify the maximum length of a line in your program (off by default)
    'consistent-return': 'warn',
    'padded-blocks': 'error',
    'prefer-template': 'warn',
    'no-template-curly-in-string': 'off',
    'no-useless-escape': 'off',
    'prefer-const': 'off',
    'no-multiple-empty-lines': [2, {
      max: 2
    }], // disallow multiple empty lines (off by default)
    'no-unused-vars': 'off',
    'comma-dangle': 'off',
    'jsx-a11y/href-no-hash': 'off',
    'jsx-a11y/anchor-is-valid': ['warn', {
      aspects: ['invalidHref']
    }],
    'no-console': 0,
    'no-script-url': 0,
    'no-var': 0,
    quotes: ['error', 'single'],
    'no-underscore-dangle': [0]
  }
};
