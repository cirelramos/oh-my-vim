"configuration cirel
set nocompatible              " be iMproved, required
filetype on   " Enable filetype detection
filetype plugin indent on    " required
filetype indent on " Enable filetype-specific indenting
filetype plugin on " Enable filetype-specific plugin
scriptencoding utf-8
set encoding=utf-8
"set bs=indent,eol,start " allow backspacing over everything in insert mode
"" Use modeline overrides
set modeline
set modelines=10
set title
set titleold="Terminal"
"set titlestring=%F
"set titlestring=%t
"set title titlestring=...%{strpart(expand(\"%:p:h\"),stridx(expand(\"%:p:h\"),\"/\",strlen(expand(\"%:p:h\"))-12))}%=%n.\ \ %{expand(\"%:t:r\")}\ %m\ %Y\ \ \ \ %l\ of\ %L
"set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\
set t_ut=
set t_ti= t_te=
if $COLORTERM == 'gnome-terminal'
 set t_Co=256
else
 set t_ut=
endif
"colorscheme molokai
colorscheme molokayo
set completeopt+=menuone            " required by deoplete
set autowrite
set number
set whichwrap+=<,>,h,l,[,]
set breakindent
set listchars=eol:↲,nbsp:␣,trail:•,extends:⟩,precedes:⟨,tab:┆·
set list
set tabstop=1 softtabstop=1 noexpandtab shiftwidth=1 smarttab shiftround
" convert tabs to spaces before writing file
"autocmd! bufwritepre * set expandtab | retab! 1
"autocmd! bufwritepost * set expandtab | retab! 1
" Searching
set incsearch       " show 'best match so far' as you type
set ignorecase " case insensitive searching
set smartcase " case-sensitive if expresson contains a capital letter
set hlsearch        " hilight the items found by the search
set grepprg =grep\ -rni\ -C\ 2\ $*\ --color=auto\ --exclude-dir={log,tmp,.git,node_modules,deps,_build}
set wildignore+=.hg,.git,.svn,vendor,node_modules                    " Version control
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg   " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest " compiled object files
set wildignore+=*.sw?                            " Vim swap files
set wildignore+=*.DS_Store                       " OSX bullshit
set wildignore+=*.pyc                            " Python byte code
set wildignore+=*.orig                           " Merge resolution files
"set visualbell                                   " flash screen when bell rings
set incsearch " set incremental search, like modern browsers
"set nolazyredraw " don't redraw while executing macros
set magic " Set magic on, for regex
set showcmd
set showmode
set showmatch " show matching braces
set laststatus=2
set selection=inclusive
set selectmode=mouse,key
"set cursorcolumn
"set cursorline
set scrolloff=7
set mat=2 " how many tenths of a second to blink
syntax on   " syntax highlight
set encoding=utf8
let base16colorspace=256  " Access colors present in 256 colorspace"
set t_Co=256 " Explicitly tell vim that the terminal supports 256 colors"
set history=7000  " keep 100 lines of command line history
set ruler   " show the cursor position all the time
set ar    " auto read when file is changed from outside
set nu    " show line Numbers
"set wildmode=longest,list
set wildmenu
set swapfile
set clipboard=unnamedplus
set guioptions+=a
"set autochdir
set dir=~/.vim/tmp/
set backup
set undodir=~/.vim/tmp/
set backupdir=~/.vim/bak/
set undofile
set undodir=~/.vim/undodir/
set foldmethod=indent
"set foldmethod=syntax
"set foldnestmax=1
set foldlevel=0
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0
let g:python_host_prog = 'usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
"let g:loaded_python_provider = 1
"let g:loaded_python3_provider = 1
"Load Plugins
"set shell=/usr/bin/zsh
command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)
set keywordprg=~/.vim/google
source ~/.vim/config-plugins-vimrc
"Configuration Plugins
source ~/.vim/config-configuration-plugins-vimrc
"Configuration Functions
source ~/.vim/config-functions-vimrc
"Configuration Shortcuts
source ~/.vim/config-shortcuts-vimrc
" auto reload vimrc when editing it
autocmd! bufwritepost .vimrc source ~/.vimrc
