FROM ubuntu:21.04
#FROM python:3.6.5-stretch
ENV DEBIAN_FRONTEND=noninteractive
#ENV DEBIAN_URL "http://ftp.us.debian.org/debian"
RUN apt-get update
RUN apt-get update && \
      apt-get -y install wget git sudo vim-gtk
ENV TZ=America/Lima
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN sudo apt-get clean && sudo apt-get update && sudo apt-get install -y locales
RUN sudo apt-get update && DEBIAN_FRONTEND=noninteractive && sudo apt-get install -y locales
RUN sudo sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sudo dpkg-reconfigure --frontend=noninteractive locales && \
    sudo update-locale LANG=en_US.UTF-8
RUN sudo apt-get install ripgrep curl pip -y
ENV LANG en_US.UTF-8

COPY quick-install.sh /tmp/quick-install.sh
RUN ls -R /root


RUN useradd -u 1000 developer
# Replace 1000 with your user / group id
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

RUN cd /tmp/ && echo "2\n 2\n developer\n 1\n" | sudo bash quick-install.sh
RUN  sudo curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

WORKDIR /mount

COPY config-plugins-vimrc /root/.vim/config-plugins-vimrc
COPY plug-install.sh /tmp/plug-install.sh
RUN cd /tmp/ && sudo bash plug-install.sh
RUN sudo ln -sf /root/.vim/.vimrc /root/.vimrc
RUN sudo ln -sf /home/developer/.vim/.vimrc /home/developer/.vimrc
COPY config-plugins-vimrc /home/developer/.vim/config-plugins-vimrc
COPY plug-install.sh /tmp/plug-install.sh
USER developer
CMD ["sudo", "vim"]

