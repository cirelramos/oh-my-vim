vimrc for UNIX Web Developers (PHP/C/C++/Lua/JavaScript/HTML/CSS)
============


Goal
----------------
This vimrc aimes to collect some useful plugins for web developers working under UNIX and using PHP, C/C++, Lua, JavaScript, etc.

I adjust the bundle list for this vimrc to focus on web development, and share with my colleagues.
I also added the chinese help documentation of VIM 7.4, see by type `:h`.






Requisites
----------------
The distribution is designed to work with Vim >= 7.4.

The distribution also requires ack, ctags, git, ruby and rake. I recommend using the GUI version of VIM (gvim on Linux and Windows, MacVim on OSX) for some plugin only support GUI version (e.g. Command-T). You can [download MacVim here](https://github.com/b4winckler/macvim/downloads).


INSTALL
-------

1. clone this vimrc
```shell
cd /tmp && sudo wget https://gitlab.com/cirelramos/oh-my-vim/raw/master/quick-install.sh -O quick-install.sh  && sudo bash quick-install.sh
```
if you intall mode docker, mode YOUR user run with
```shell
sudo docker run -it -v $PWD:/mount  -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.Xauthority:/home/developer/.Xauthority --net=host --rm cirelramos/oh-my-vim-install vim
```

if you intall mode docker, mode ROOT user run with
```shell
sudo docker run -it -v $PWD:/mount  -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.Xauthority:/home/developer/.Xauthority --net=host --rm cirelramos/oh-my-vim-install
```


2. Run the install npm
```shell
sudo su
```
```shell
npm install -g react-native-cli babel-eslint esformatter-quotes eslint-plugin-babel eslint-plugin-jsx-a11y node-gyp webpack csslint eslint eslint-plugin-import eslint-plugin-react npm-check-updates esformatter eslint-config-airbnb eslint-plugin-jest jshint
```
2. Run vim and install the plugins with
```shell
:PlugInstall
```
> Notice: This step may cost much longer time for configuring .

3. Finished! Enjoy it :-)


Conventions
------------

* Backup enabled, the BAK file is saved as `~/.vim/bak/$NAME~`

* Swap file is moved to `~/.vim/tmp/$NAME.swp`

* Shourtcuts, the `<leader>` key is `-`
  - `-w` Save
  - `-q` Force to quit without saving
  - `F7` `-t` Toggle Tagbar
  - `F5` `-n` Toggle NerdTree Tabs
  - `F6` `-sy` Manually Syntax check by syntastic plugin
  - `F4` `-y` AutoFormat
  - `Ctrl-P` Invoke CtrlP to find files




Useful vim Plugins included
----------------

 * [vim-easytag](https://github.com/xolox/vim-easytags)
 * [tagbar](https://github.com/majutsushi/tagbar)
 * [tagbar-phpctags](https://github.com/techlivezheng/tagbar-phpctags)
 * [nerdtree](https://github.com/scrooloose/nerdtree)
 * [nerdcommenter.git](https://github.com/scrooloose/nerdcommenter.git)



## Vim tips

![vim cheat sheet](vi-vim-cheat-sheet.gif)

