#!/bin/bash
cd /home/developer
if [ -d /home/developer/.vim/version_Dockerfile ]; then
    echo "Folder exists"
else
  cd /home/developer
  sudo rm -rf /home/developer/.vim
  sudo git clone https://gitlab.com/cirelramos/oh-my-vim.git .vim
  sudo chmod 777 -R /home/developer/.vim
fi

sudo vim $@

