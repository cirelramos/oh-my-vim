#!/bin/bash
function downloadRepo(){
  if [ -d "/tmp/.oh-my-vim" ]; then
    rm -rf /tmp/.oh-my-vim/
  fi
  cd /tmp/
  echo $path" <- directorio"
  if [ ! -d "/tmp/.oh-my-vim" ]; then
    git clone https://gitlab.com/cirelramos/oh-my-vim.git
    mv oh-my-vim/ .oh-my-vim/
  fi
}
function cpFiles(){
  echo "cp files"
  cd .oh-my-vim/
  mkdir -p $rute/.vim/
  yes | cp -rf .??* $rute/.vim/
  yes | cp -rf * $rute/.vim/
  echo "cp config"
  #ln -sf $rute/.vim/.vimrc $rute/.vimrc
  ln -sf $rute/.vim/.editorconfig $rute/.editorconfig
  ln -sf $rute/.vim/.vimrc $rute/.vim/init.vim
  chmod 777 -R $rute/.vim/
  echo "#nvim symbolic link as $userLog"
  rm -r $rute/.config/nvim
  ln -s $rute/.vim/ $rute/.config/nvim
  if [ $userLog != "root" ]; then
    echo "rm config vim root"
    rm -r /root/.vim
    rm /root/.vimrc
    echo "create link .vim you user to root"
    mkdir /root/.vim/
    yes | cp -rf .??* /root/.vim/
    yes | cp -rf * /root/.vim/
    rm -r /root/.vim/.eslintrc.js
    ln -sf $rute/.vim/.eslintrc.js /root/.vim/.eslintrc.js
    rm -r /root/.vim/plugged
    ln -sf $rute/.vim/plugged /root/.vim/plugged
    rm -r /root/.vim/bundle
    ln -sf $rute/.vim/bundle /root/.vim/bundle
    rm -r /root/.vim/config-configuration-plugins-vimrc
    ln -sf $rute/.vim/config-configuration-plugins-vimrc /root/.vim/config-configuration-plugins-vimrc
    rm -r /root/.vim/config-lines-backups-comments-vimrc
    ln -sf $rute/.vim/config-lines-backups-comments-vimrc /root/.vim/config-lines-backups-comments-vimrc
    rm -r /root/.vim/config-plugins-vimrc
    ln -sf $rute/.vim/config-plugins-vimrc /root/.vim/config-plugins-vimrc
    rm -r /root/.vim/config-shortcuts-vimrc
    ln -sf $rute/.vim/config-shortcuts-vimrc /root/.vim/config-shortcuts-vimrc
    echo "create link config .vimrc you user to root"
    ln -sf $rute/.vim/.editorconfig /root/.editorconfig
    echo "#nvim symbolic link as root"
    rm -r /root/.config/nvim
    ln -sf /root/.vim/ /root/.config/nvim
  else
    echo "$(whoami)"
  fi
}

function installNormal(){
  userLog=$(logname)
  if [ "$userLog"=="" ]; then
    if [ $SUDO_USER ]; then userLog=$SUDO_USER; else userLog=`whoami`; fi
    userLog=$(w | head -n3 | tail -n 1 | cut -d " " -f1)
  fi
  echo -e "\n 1)server o computer  2)container"
  echo -n "Select a type device  [ENTER]: "
  read -s valueDevice
  if [ ! -z "$valueDevice" ]; then
    case $valueDevice in
      "1")
        ;;
      "2")
        echo -n "type a user  [ENTER]: "
        read -s valueUser
        if [ ! -z "$valueUser" ]; then
          userLog=$valueUser
        fi
        ;;
    esac
  fi
  echo "==============================="$userLog"==============================="
  rute="/home/$userLog"
  path="/home/$userLog/.vim"
  if [ $userLog != "root" ]; then
    echo "no root"
  else
    rute="/$userLog"
    path="$rute/.vim"
  fi
  echo -e "\n 1)Install 2)Update"
  echo -n "Select a service [ENTER]: "
  read -s value
  if [ ! -z "$value" ]; then
    case $value in
      "1")
        if [ ! -f "/usr/bin/nodejs" ] && [  -f "/usr/bin/pacman" ]; then
          yes | pacman -S pkg-config cmake net-tools php nodejs npm ctags gvim ruby ttf-dejavu kbd  python-neovim neovim awesome-terminal-fonts ripgrep
          ln -s /usr/bin/ctags /usr/local/bin/ctags
          #cd ~/.vim/plugged/command-t/ruby/command-t/ext/command-t
          #ruby extconf.rb && make
        fi
        php_install=""
        gvim_install=""
        node_install=""
        if [ ! -f "/usr/bin/nodejs" ] && [  -f "/usr/bin/apt-get" ]; then
          node_install="software-properties-common curl zip unzip ncurses-dev git"
        fi
        if [ ! -f "/usr/bin/nodejs" ] && [  -f "/usr/bin/apt-get" ]; then
          if [ $valueDevice != "2" ]; then
            gvim_install="gvim"
          fi
          distribution=$(cat /etc/*-release | grep -ionc "jessie")
          if [ "$distribution" != "0"  ] ; then
            php_install="php5"
          else
            php_install="php"
          fi

          apt-get update
          cd ~
          apt-get install -y dh-autoreconf autogen pkg-config python-dev python-pip python3-dev  python3-pip python-dev python-pip python3-dev python3-setuptools python-dev python-pip python3-dev python3-pip python-setuptools build-essential cmake python-dev python3-dev python-dev build-essential python-autopep8 vim-rails python-greenlet autoconf automake cmake fish g++ gettext git libtool libtool-bin lua5.3 ninja-build pkg-config unzip xclip xfonts-utils $php_install $gvim_install $node_install
          curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
          apt-get install -y nodejs npm
          bash nodesource_setup.sh
          npm -g update
          apt-get update

          apt --fix-broken install python-pycurl python-apt -y

          pip install --upgrade setuptools pip
          pip3 install neovim
        fi
        userP=$(who | awk '{print $1}' | sort | uniq)
        userP=$userLog
        distributionUbuntu=$(uname -a | grep -ic "ubuntu")
        golangExist=$(grep -rion "golang" /etc/apt/sources.list.d/ | wc -l)
        vimExist=$(grep -rion "jonathonf" /etc/apt/sources.list.d/ | wc -l)
        mkdir -p /opt/vim/
        chown "$userP":root -R /opt/vim/
        chmod 775 -R /opt/vim/
        cd /opt/vim/
        if [ "$distributionUbuntu" != "0"  ]; then
          if [ 0 -eq "$vimExist"  ]; then
            add-apt-repository ppa:jonathonf/vim -y
          fi
        fi
        if [ ! -f "php-cs-fixer.phar" ]; then
          wget -c https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v2.10.0/php-cs-fixer.phar
          wget -c https://gitlab.com/cirelramos/oh-my-vim/raw/master/twig-lint.phar
          yes | cp -rf $rute/.vim/phpctags /opt/vim/phpctags
          yes | cp -rf php-cs-fixer.phar php-cs-fixer
          chmod a+x php-cs-fixer
          mv php-cs-fixer /usr/local/bin/php-cs-fixer
        fi
        cd $rute/
        mkdir $rute/.vim/
        directory="$(pwd)"
        rm -r /opt/vim/master*
        downloadRepo
        cpFiles
        if [ ! -d "$rute/.vim/bundle/" ]; then
          chown "$userP":root -R $rute/.vim/
          chmod 775 -R $rute/.vim/
          cd $rute/.vim/bundle &&  git clone --depth=1 https://github.com/vim-syntastic/syntastic.git
        fi
        cd /opt/vim/
        cd /opt/vim/
        if [ ! -f "/usr/bin/uglifyjsEs6" ]; then
          npm install npm@latest -g.
          npm install -g react-native-cli babel-eslint esformatter-quotes eslint-plugin-babel eslint-plugin-jsx-a11y node-gyp webpack csslint eslint eslint-plugin-import eslint-plugin-react npm-check-updates esformatter eslint-config-airbnb eslint-plugin-jest jshint typescript minifier cssgzmin unminify cssunminifier js-beautify uglify-js eslint-plugin-vue eslint
          ln -s /usr/lib/node_modules/uglify-js/bin/uglifyjs /usr/bin/uglifyjsEs5
          npm install -g uglify-js-es6
          ln -s /usr/lib/node_modules/uglify-js-es6/bin/uglifyjs /usr/bin/uglifyjsEs6
        fi
        ;;
      "2")
        downloadRepo
        cpFiles
        ;;
    esac
  fi
}

function dockerInstall(){
  downloadRepo
  cd /tmp/.oh-my-vim/
  docker build . -t oh-my-vim
  echo -e ""
  echo -e "now you can run"
  echo -e "docker run -it -v \$PWD:/mount --rm oh-my-vim vim"
}

echo -e "\n 1)docker  2)normal"
echo -n "Select a type device  [ENTER]: "
read -s typeInstall
if [ ! -z "$typeInstall" ]; then
  case $typeInstall in
    "1")
      dockerInstall
      ;;
    *)
      installNormal
      ;;
  esac
fi
